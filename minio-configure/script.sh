set -e
if [ -z "$MINIO_ROOT_USER" ] && [ ! -z "$MINIO_ROOT_USER_FILE" ]
then
	MINIO_ROOT_USER=$(cat $MINIO_ROOT_USER_FILE)
fi
if [ -z "$MINIO_ROOT_PASSWORD" ] && [ ! -z "$MINIO_ROOT_PASSWORD_FILE" ]
then
	MINIO_ROOT_PASSWORD=$(cat $MINIO_ROOT_PASSWORD_FILE)
fi
[[ -z "$MINIO_URL" ]] && { echo "MINIO_URL required"; exit 1; }
[[ -z "$MINIO_ROOT_USER" ]] && { echo "MINIO_ROOT_USER required"; exit 1; }
[[ -z "$MINIO_ROOT_PASSWORD" ]] && { echo "MINIO_ROOT_PASSWORD required"; exit 1; }
MINIO_HEALTH_CHECK_URL="${MINIO_URL}/minio/health/ready"
echo "wait for: $MINIO_HEALTH_CHECK_URL"
wget -qO- https://raw.githubusercontent.com/eficode/wait-for/v2.2.2/wait-for | sh -s -- $MINIO_HEALTH_CHECK_URL -t 10
if ( ! test -x "$(which mc)" ); then
	wget -q -O /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc
	chmod +x /usr/local/bin/mc
fi
mc alias set minio $MINIO_URL $MINIO_ROOT_USER $MINIO_ROOT_PASSWORD
echo "starting:${1}"
node $1
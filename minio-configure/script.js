const env = process.env;
const {
	spawnSync
} = require('child_process');
const assert = require('assert');
const fs = require('fs');
const DELIM = "_";
const BUCKET_PREFIX = `MINIO${DELIM}BUCKETS${DELIM}`;
const USER_PREFIX = `MINIO${DELIM}USERS${DELIM}`;
const READ_WRITE_BUCKET_POLICY_TEMPLATE_PATH = '/read-write-bucket-policy-template.json';
const bucketIndexes = new Set();
const userIndexes = new Set();
const DEBUG = env["DEBUG"] != null && "true" == env["DEBUG"].toLowerCase();

function isBlank(str) {
	return (!str || /^\s*$/.test(str));
}

function mc() {
	procArguments = [...arguments];
	const proc = spawnSync('mc', procArguments, {
			env: env
		});
	var stdout = proc.stdout.toString().trim();
	var stderr = proc.stderr.toString().trim();
	if (DEBUG)
		console.log(`stdout:\n${stdout}`);
	if (DEBUG)
		console.log(`stderr:\n${stderr}`);
	for (var out of[stdout, stderr]) {
		if (!isBlank(out)) {
			try {
				return JSON.parse(out);
			} catch (e) {
				var success = out.toLowerCase().indexOf('success') >= 0;
				return {
					status: `${success?'success':'error'}`,
					message: out
				};
			}
		}
	}
	return null;
}

function mcUserAdd(username, password) {
	if (mc('admin', 'user', 'info', 'minio', username, '--json').status != "success") {
		var result = mc('admin', 'user', 'add', 'minio/', username, password, '--json');
		assert(result.status == "success");
		console.log(`user added:${username}`);
	} else
		console.log(`user exists:${username}`);
}

function mcBucketCreate(bucketName) {
	if (mc('stat', `minio/${bucketName}`, '--json').status != "success") {
		var result = mc('mb', '-p', `minio/${bucketName}`);
		assert(result.status == "success");
		console.log(`bucket added:${bucketName}`);
	} else
		console.log(`bucket exists:${bucketName}`);
}

function mcPolicySet(username, policy) {
	var result = mc('admin', 'policy', 'set', 'minio', policy, `user=${username}`, '--json');
	assert(result.status == "success");
	console.log(`policy ${policy} set:${username}`);
}

function mcPolicyReadWriteBucketCreate(bucketName) {
	var policyName = `readwrite_${bucketName}`;
	var policyTemplate = fs.readFileSync(READ_WRITE_BUCKET_POLICY_TEMPLATE_PATH, "utf8");
	var policy = policyTemplate.replaceAll("${BUCKET_NAME}", bucketName);
	var fileName = `policy_${Buffer.from(Math.random().toString()).toString("base64").substr(10, 5)}.json`;
	if (DEBUG)
		console.log(fileName);
	try {
		fs.writeFileSync(fileName, policy, {
			encoding: "utf8"
		});
		var result = mc('admin', 'policy', 'add', `minio/`, policyName, fileName, '--json');
	}
	finally {
		fs.unlinkSync(fileName)
	}
	assert(result.status == "success");
	console.log(`policy ${policyName} added`);
	return policyName;
}

Object.keys(env).forEach(key => {
	if (key.startsWith(BUCKET_PREFIX)) {
		key = key.substring(BUCKET_PREFIX.length);
		indexes = bucketIndexes;
	} else if (key.startsWith(USER_PREFIX)) {
		key = key.substring(USER_PREFIX.length);
		indexes = userIndexes;
	} else
		return;
	splitAt = key.indexOf(DELIM);
	var index;
	if (splitAt < 0)
		index = -1;
	else
		index = parseInt(key.substring(0, splitAt));
	if (isNaN(index))
		return;
	indexes.add(index);
});

function readValue(prefix, index, key) {
	var indexDelim = index == -1 ? "" : `${index}${DELIM}`;
	var envKey = `${prefix}${indexDelim}${key}`;
	var value = env[envKey];
	if (DEBUG)
		console.log(`${envKey}:${value}`);
	return value;
}

function processBucketIndex(index) {
	var bucketName = readValue(BUCKET_PREFIX, index, "NAME");
	if (isBlank(bucketName))
		return;
	mcBucketCreate(bucketName);
}

for (var index of bucketIndexes) {
	processBucketIndex(index);
}

function processUserIndex(index) {
	var username = readValue(USER_PREFIX, index, "USER");
	if (isBlank(username))
		return;
	var password = readValue(USER_PREFIX, index, "PASSWORD");
	if (isBlank(password))
		return;
	mcUserAdd(username, password);
	var readWrite = readValue(USER_PREFIX, index, "READ_WRITE");
	if (!isBlank(readWrite) && 'true' == readWrite.toLowerCase()) {
		mcPolicySet(username, 'readwrite');
	}
	var readWriteBucket = readValue(USER_PREFIX, index, "READ_WRITE_BUCKET");
	if (!isBlank(readWriteBucket)) {
		mcBucketCreate(readWriteBucket);
		var policyName = mcPolicyReadWriteBucketCreate(readWriteBucket);
		mcPolicySet(username, policyName);
	}
}

for (var index of userIndexes) {
	processUserIndex(index);
}

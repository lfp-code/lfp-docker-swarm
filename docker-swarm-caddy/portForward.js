{
  "storage": {
    "aes_key": "$${STORAGE_AES_KEY}",
    "db": 0,
    "host": "$${STORAGE_HOST}",
    "key_prefix": "$${STORAGE_KEY_PREFIX}",
    "module": "redis",
    "password": "$${STORAGE_PASSWORD}",
    "port": "$${STORAGE_PORT}",
    "timeout": 5,
    "tls_enabled": false,
    "tls_insecure": true
  },
  "apps": {
    "dynamic_dns": {
      "check_interval": 60000000000,
      "dns_provider": {
        "name": "route53"
      },
      "domains": {
        "$${DYNAMIC_DNS_DOMAIN}": [
          "$${SUB_DOMAIN}"
        ]
      },
      "ip_sources": [
        {
          "source": "upnp"
        },
        {
          "endpoints": [
            "https://api.ipify.org"
          ],
          "source": "simple_http"
        },
        {
          "endpoints": [
            "https://api64.ipify.org"
          ],
          "source": "simple_http"
        }
      ]
    },
    "tls": {
      "certificates": {
        "automate": [
          "$${SUB_DOMAIN}.$${DOMAIN}"
        ]
      },
      "automation": {
        "policies": [
          {
            "issuers": [
              {
                "api_key": "$${ZERO_SSL_API_KEY}",
                "challenges": {
                  "dns": {
                    "provider": {
                      "name": "route53"
                    }
                  }
                },
                "email": "$${EMAIL}",
                "module": "zerossl"
              }
            ]
          }
        ]
      }
    },
    "layer4": {
      "servers": {
        "proxy": {
          "listen": [
            ":$${LISTEN_PORT}"
          ],
          "routes": [
            {
              "match": [
                {
                  "tls": {
                    "sni": [
                      "$${SUB_DOMAIN}.$${DOMAIN}"
                    ]
                  }
                }
              ],
              "handle": [
                {
                  "handler": "tls"
                },
                {
                  "handler": "proxy",
                  "upstreams": [
                    {
                      "dial": [
                        "$${DIAL_ADDRESS}"
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    }
  }
}
set -e
MINIO_HEALTH_CHECK_URL="${MINIO_URL}/minio/health/ready"
echo "wait for: $MINIO_HEALTH_CHECK_URL"
wget -qO- https://raw.githubusercontent.com/eficode/wait-for/v2.2.2/wait-for | sh -s -- $MINIO_HEALTH_CHECK_URL -t 10
if [[ $? -ne 0 ]]; then
	echo "wait failed:${MINIO_HEALTH_CHECK_URL}"
	exit 1
fi
if ( ! test -x "$(which mc)" ); then
	wget -q -O /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/mc
	chmod +x /usr/local/bin/mc
fi
MINIO_ROOT_USER=$(cat /var/run/secrets/cluster_MINIO_ROOT_USER)
MINIO_MINIO_ROOT_PASSWORD=$(cat /var/run/secrets/cluster_MINIO_ROOT_PASSWORD)
./mc alias set minio $MINIO_URL $MINIO_ROOT_USER $MINIO_MINIO_ROOT_PASSWORD
./mc admin user add minio/ $MINIO_VOLUME_ACCESS_KEY $MINIO_VOLUME_SECRET_KEY
./mc admin policy set minio readwrite user=$MINIO_VOLUME_ACCESS_KEY

# export MINIO_URL=https://minio.iplasso.com
# mkdir -p /var/run/secrets
# echo "PFPG0SR0JA2QGEYEMZ8G" > /var/run/secrets/cluster_MINIO_ROOT_USER
# echo "G+OpAas+nC7uHf1Kr62CjP9hMpCJxcWG2CiQCCnq" > /var/run/secrets/cluster_MINIO_ROOT_PASSWORD
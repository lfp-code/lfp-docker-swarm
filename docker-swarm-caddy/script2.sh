set -e
if ( ! test -x "$(which s3fs)" || ! test -x "$(which wget)" || ! test -x "$(which nc)" || ! test -x "$(which alien)" )
then
	apt-get update
	apt-get install -y s3fs wget netcat alien
fi
MINIO_HEALTH_CHECK_URL="${MINIO_URL}/minio/health/ready"
echo "wait for: $MINIO_HEALTH_CHECK_URL"
wget -qO- https://raw.githubusercontent.com/eficode/wait-for/v2.2.2/wait-for | sh -s -- $MINIO_HEALTH_CHECK_URL -t 10
if [[ $? -ne 0 ]]; then
	echo "wait failed:${MINIO_HEALTH_CHECK_URL}"
	exit 1
fi
if [[ ! -e "./mc" ]]; then
	wget -q -O mc https://dl.min.io/client/mc/release/linux-amd64/mc
	chmod +x mc
fi
MINIO_ROOT_USER=$(cat /var/run/secrets/cluster_MINIO_ROOT_USER)
MINIO_MINIO_ROOT_PASSWORD=$(cat /var/run/secrets/cluster_MINIO_ROOT_PASSWORD)
./mc alias set minio $MINIO_URL $MINIO_ROOT_USER $MINIO_MINIO_ROOT_PASSWORD

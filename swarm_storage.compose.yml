version: '3.7'

x-minio-root-secrets: &minio-root-secrets
    secrets:
      - cluster_MINIO_ROOT_USER
      - cluster_MINIO_ROOT_PASSWORD

x-minio-configure-users: &minio-configure-users
      MINIO_USERS_0_USER: ${MINIO_VOLUME_ACCESS_KEY?Variable not set}
      MINIO_USERS_0_PASSWORD: ${MINIO_VOLUME_SECRET_KEY?Variable not set}
      MINIO_USERS_0_READ_WRITE: "true"
      MINIO_USERS_1_USER: ${MINIO_CLUSTER_REDIS_ACCESS_KEY?Variable not set}
      MINIO_USERS_1_PASSWORD: ${MINIO_CLUSTER_REDIS_SECRET_KEY?Variable not set}
      MINIO_USERS_1_READ_WRITE_BUCKET: cluster-redis

x-minio-base: &minio-base
    image: 'bitnami/minio:latest'
    environment:
      - "MINIO_BROWSER_REDIRECT_URL=https://console-minio.${DOMAIN?Variable not set}"
      - "MINIO_ROOT_USER_FILE=${MINIO_ROOT_USER_FILE:-/var/run/secrets/cluster_MINIO_ROOT_USER}"
      - "MINIO_ROOT_PASSWORD_FILE=${MINIO_ROOT_PASSWORD_FILE:-/var/run/secrets/cluster_MINIO_ROOT_PASSWORD}"
      - MINIO_DISTRIBUTED_MODE_ENABLED=yes
      - MINIO_DISTRIBUTED_NODES=minio-{0...2}/data-{0...1}
      - MINIO_SKIP_CLIENT=yes
      - BITNAMI_DEBUG=true
    networks:
      - minio-internal
    labels:
      caddy-minio_0: ":9000"
      caddy-minio_0.reverse_proxy: "{{upstreams 9000}}"
      caddy-minio_1: ":9001"
      caddy-minio_1.reverse_proxy: "{{upstreams 9001}}"
    <<: *minio-root-secrets

x-delayed-on-failure: &delayed-on-failure
      restart_policy:
        condition: on-failure
        delay: 10s
      update_config:
        parallelism: 10
        delay: 10s

x-caddy-redis-proxy-service-config: &caddy-redis-proxy-service-config
    #https://csvjson.com/json_beautifier
      CADDY_CONFIG: |
        {
          "storage": {
            "module": "redis",
            "aes_key": "$${STORAGE_AES_KEY?Variable not found}",
            "tls_enabled": false,
            "host": "$${STORAGE_HOST?Variable not found}",
            "port": "$${STORAGE_PORT?Variable not found}",
            "password": "$${STORAGE_PASSWORD?Variable not found}",
            "key_prefix": "$${STORAGE_KEY_PREFIX?Variable not found}",
            "db": 0,
            "timeout": 5
          },
          "apps": {
            "dynamic_dns": {
              "check_interval": 60000000000,
              "dns_provider": {
                "name": "route53"
              },
              "domains": {
                "$${DYNAMIC_DNS_DOMAIN}": [ "$${SUB_DOMAIN}" ]
              },
              "ip_sources": [
                {
                  "source": "upnp"
                },
                {
                  "endpoints": [ "https://api.ipify.org" ],
                  "source": "simple_http"
                },
                {
                  "endpoints": [ "https://api64.ipify.org" ],
                  "source": "simple_http"
                }
              ]
            },
            "tls": {
              "certificates": {
                "automate": [ "$${SUB_DOMAIN}.$${DOMAIN}" ]
              },
              "automation": {
                "policies": [
                  {
                    "issuers": [
                      {
                        "api_key": "$${ZERO_SSL_API_KEY}",
                        "challenges": {
                          "dns": {
                            "provider": {
                              "name": "route53"
                            }
                          }
                        },
                        "email": "$${EMAIL}",
                        "module": "zerossl"
                      }
                    ]
                  }
                ]
              }
            },
            "layer4": {
              "servers": {
                "proxy": {
                  "listen": [ ":$${LISTEN_PORT}" ],
                  "routes": [
                    {
                      "match": [ { "tls": { "sni": [ "$${SUB_DOMAIN}.$${DOMAIN}" ] } } ],
                      "handle": [
                        {
                          "handler": "tls"
                        },
                        {
                          "handler": "proxy",
                          "upstreams": [ { "dial": [ "$${DIAL_ADDRESS}" ] } ]
                        }
                      ]
                    }
                  ]
                }
              }
            }
          }
        }

services:

  minio-0:
    <<: *minio-base
    #BELOW CHANGES PER NODE
    hostname: minio-0
    volumes:
      - 'minio_0_data_0:/data-0'
      - 'minio_0_data_1:/data-1'
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true
          - node.labels.minio-0==true

  minio-1:
    <<: *minio-base
    #BELOW CHANGES PER NODE
    hostname: minio-1
    volumes:
      - 'minio_1_data_0:/data-0'
      - 'minio_1_data_1:/data-1'
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true
          - node.labels.minio-1==true

  minio-2:
    <<: *minio-base
    #BELOW CHANGES PER NODE
    hostname: minio-2
    volumes:
      - 'minio_2_data_0:/data-0'
      - 'minio_2_data_1:/data-1'
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true
          - node.labels.minio-2==true

  minio-configure:
    image: regbo/minio-configure:latest
    environment:
      MINIO_URL: http://minio:9000
      MINIO_ROOT_USER_FILE: ${MINIO_ROOT_USER_FILE:-/var/run/secrets/cluster_MINIO_ROOT_USER}
      MINIO_ROOT_PASSWORD_FILE: ${MINIO_ROOT_PASSWORD_FILE:-/var/run/secrets/cluster_MINIO_ROOT_PASSWORD}
      <<: *minio-configure-users
    networks:
      - cluster-storage
    <<: *minio-root-secrets
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true
      <<: *delayed-on-failure


  caddy-service:
    image: regbo/docker-swarm-caddy:latest
    ports:
      - published: 9000
        target: 9000
        protocol: tcp
        mode: host
    environment:
      CADDY_DOCKER_MODE:  standalone
      CADDY_INGRESS_NETWORKS: cluster-caddy
      CADDY_DOCKER_LABEL_PREFIX: caddy
      CADDY_DOCKER_CADDYFILE_PATH: ./Caddyfile
      CADDY_DOCKER_DEBUG: ${CADDY_DOCKER_DEBUG:-false}
      AWS_ACCESS_KEY_ID: ${ROUTE53_ACCESS_KEY_CADDY?Variable not set}
      AWS_SECRET_ACCESS_KEY: ${ROUTE53_SECRET_KEY_CADDY?Variable not set}
      #for now requires trailing dot on route53 see https://github.com/libdns/route53/issues/10
      #DYNAMIC_DNS_DOMAIN: ${DOMAIN?Variable not set}
      DYNAMIC_DNS_DOMAIN: ${DOMAIN?Variable not set}.
      COMMAND_SH: |
        export REDIS_PASSWORD=`cat /var/run/secrets/cluster_REDIS_PASSWORD`
        if( echo $$CADDY_DOCKER_DEBUG | grep -i "^true$$" > /dev/null )
        then
          echo -e "{\n debug \n}" > $$CADDY_DOCKER_CADDYFILE_PATH
        else
          echo -e "{\n}" > $$CADDY_DOCKER_CADDYFILE_PATH
        fi
        caddy fmt --overwrite $$CADDY_DOCKER_CADDYFILE_PATH
        caddy docker-proxy
    secrets:
      - cluster_REDIS_PASSWORD
    command:
      - sh
      - -c
      - "echo \"$$COMMAND_SH\" > ./command.sh && cat ./command.sh && chmod +x ./command.sh && ./command.sh"
    networks:
      - cluster-caddy
      - cluster-storage
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - caddy-data:/data
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true
      labels:
        caddy.email: ${EMAIL?Variable not set}
        #STORAGE
        caddy.storage: redis
        caddy.storage.host: cluster-redis
        caddy.storage.port: 6379
        caddy.storage.password: "{$$REDIS_PASSWORD}"
        caddy.storage.db: 0
        caddy.storage.timeout: 5
        caddy.storage.key_prefix: cluster_caddy_storage_
        caddy.storage.tls_enabled: "false"
        caddy.storage.tls_insecure: "true"
        caddy.storage.aes_key: ${CADDY_REDIS_AES_KEY?Variable not set}
        #CERTIFICATES
        caddy.cert_issuer: zerossl ${ZERO_SSL_API_KEY?Variable not set}
        caddy.acme_dns: route53
        #DYNAMIC DNS
        caddy.dynamic_dns.provider: route53
        caddy.dynamic_dns.check_interval: 1m
        #DOES NOT NEED DOUBLE $!
        caddy.dynamic_dns.domains.{$DYNAMIC_DNS_DOMAIN}: cluster *.cluster
        caddy.dynamic_dns.0_ip_source: upnp
        caddy.dynamic_dns.1_ip_source: simple_http https://api.ipify.org
        caddy.dynamic_dns.2_ip_source: simple_http https://api64.ipify.org

  caddy-minio-service:
    image: regbo/docker-swarm-caddy:latest
    hostname: minio
    environment:
      CADDY_DOCKER_MODE:  standalone
      CADDY_DOCKER_LABEL_PREFIX: caddy-minio
      CADDY_DOCKER_CADDYFILE_PATH: ./Caddyfile
      COMMAND_SH: |
        if( echo $$CADDY_DOCKER_DEBUG | grep -i "^true$$" > /dev/null )
        then
          echo -e "{\n auto_https off \n debug \n}" > $$CADDY_DOCKER_CADDYFILE_PATH
        else
          echo -e "{\n auto_https off \n}" > $$CADDY_DOCKER_CADDYFILE_PATH
        fi
        caddy fmt --overwrite $$CADDY_DOCKER_CADDYFILE_PATH
        caddy docker-proxy
    command:
      - sh
      - -c
      - "echo \"$$COMMAND_SH\" > ./command.sh && cat ./command.sh && chmod +x ./command.sh && ./command.sh"
    networks:
      - cluster-caddy
      - cluster-storage
      - minio-internal
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    deploy:
      mode: global
      placement:
        constraints:
          - node.labels.storage==true
      labels:
        caddy: minio.cluster.${DOMAIN?Variable not set}:9000 console.minio.cluster.${DOMAIN?Variable not set}:9000

        caddy.0_@api: host minio.cluster.${DOMAIN?Variable not set}
        caddy.0_handle: "@api"
        caddy.0_handle.reverse_proxy: "{{upstreams 9000}}"

        caddy.1_@console: host console.minio.cluster.${DOMAIN?Variable not set}
        caddy.1_handle: "@console"
        caddy.1_handle.reverse_proxy: "{{upstreams 9001}}"

  caddy-redis-proxy-service:
    image: regbo/docker-swarm-caddy:latest
    ports:
      - published: 6379
        target: 6379
        protocol: tcp
        mode: host
    environment:
      EMAIL: ${EMAIL?Variable not set}
      AWS_ACCESS_KEY_ID: ${ROUTE53_ACCESS_KEY_CADDY?Variable not set}
      AWS_SECRET_ACCESS_KEY: ${ROUTE53_SECRET_KEY_CADDY?Variable not set}
      ZERO_SSL_API_KEY: ${ZERO_SSL_API_KEY?Variable not set}
      STORAGE_HOST: cluster-redis
      STORAGE_PORT: 6379
      STORAGE_KEY_PREFIX: ${CADDY_REDIS_PROXY_SERVICE_STORAGE_KEY_PREFIX:-cluster_caddy_redis_proxy_service_storage_}
      STORAGE_AES_KEY: ${CADDY_REDIS_PROXY_SERVICE_STORAGE_AES_KEY?Variable not set}
      SUB_DOMAIN: redis.cluster
      DOMAIN: ${DOMAIN?Variable not set}
      #for now requires trailing dot on route53 see https://github.com/libdns/route53/issues/10
      #DYNAMIC_DNS_DOMAIN: ${DOMAIN?Variable not set}
      DYNAMIC_DNS_DOMAIN: ${DOMAIN?Variable not set}.
      LISTEN_PORT: 6379
      DIAL_ADDRESS: cluster-redis:6379
      <<: *caddy-redis-proxy-service-config
    command:
      - bash
      - -c
      - |
        export STORAGE_PASSWORD=`cat /var/run/secrets/cluster_REDIS_PASSWORD`
        echo "$$CADDY_CONFIG" > ./caddy.template.json
        envsubst < ./caddy.template.json > ./caddy.json
        caddy run --config ./caddy.json
    secrets:
      - cluster_REDIS_PASSWORD
    networks:
      - cluster-storage
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true

  redis-service:
    image: 'regbo/script-exec'
    environment:
      S3_URL: http://minio:9000
      S3_ACCESS_KEY: ${MINIO_CLUSTER_REDIS_ACCESS_KEY?Variable not set}
      S3_SECRET_KEY: ${MINIO_CLUSTER_REDIS_SECRET_KEY?Variable not set}
      S3_BUCKET: cluster-redis
      CONTAINER_NAME: "cluster-redis_dind_{{.Node.ID}}"
      CONTAINER_HOSTNAME: "cluster-redis_dind_{{.Node.ID}}"
      STORAGE_NETWORK: cluster-storage
      REDIS_PASSWORD_FILE: ${REDIS_PASSWORD_FILE:-/var/run/secrets/cluster_REDIS_PASSWORD}
    secrets:
      - cluster_REDIS_PASSWORD
    command:
      - |
        echo 'starting port forwarding'
        socat TCP-LISTEN:6379,fork,reuseaddr TCP:$$CONTAINER_HOSTNAME:6379 &
        echo 'starting redis container'
        env > env.list
        docker rm -f "$${CONTAINER_NAME}"
        docker pull regbo/redis-s3fs:latest
        docker run \
          --rm \
          --name "$${CONTAINER_NAME}" \
          --hostname "$${CONTAINER_HOSTNAME}" \
          --device /dev/fuse \
          --cap-add SYS_ADMIN \
          --security-opt "apparmor=unconfined" \
          --env-file env.list \
          --user root \
          --network "$${STORAGE_NETWORK}" \
          regbo/redis-s3fs:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    hostname: cluster-redis
    networks:
      - cluster-storage
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.storage==true

  consul-service:
    image: sdelrio/consul:latest
    networks:
        - cluster-storage
    environment:
        - 'CONSUL_LOCAL_CONFIG={"skip_leave_on_interrupt": true}'
        - CONSUL_BIND_INTERFACE=eth0
        - CONSUL=consul
        - CONSUL_CHECK_LEADER=true
    deploy:
      mode: global
      placement:
        constraints: [node.role == manager]
      restart_policy:
        condition: on-failure
        delay: 20s
        max_attempts: 3
        window: 120s
      update_config:
        parallelism: 1
        delay: 10s
        failure_action: continue

  init-minio-data:
    image: 'regbo/docker-swarm-host-command:latest'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      DOCKER_HOST_COMMAND_DEBUG : ${DOCKER_HOST_COMMAND_DEBUG:-false}
      DOCKER_HOST_COMMAND: |
        mkdir -p /mnt/docker/cluster/minio/data-0
        mkdir -p /mnt/docker/cluster/minio/data-1
        chown -R 1001:1001 /mnt/docker/cluster/minio/data-0
        chown -R 1001:1001 /mnt/docker/cluster/minio/data-1
    deploy:
      mode: global
      placement:
        constraints:
          - node.labels.storage==true
      <<: *delayed-on-failure

  init-caddy-data:
    image: 'regbo/docker-swarm-host-command:latest'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      DOCKER_HOST_COMMAND_DEBUG : ${DOCKER_HOST_COMMAND_DEBUG:-false}
      DOCKER_HOST_COMMAND: |
        mkdir -p /mnt/docker/cluster/caddy
    deploy:
      mode: global
      placement:
        constraints:
          - node.labels.storage==true
      <<: *delayed-on-failure

  init-s3fs:
    image: 'regbo/script-exec'
    environment:
      S3_URL: https://minio.cluster.${DOMAIN?Variable not set}:9000
      S3_ACCESS_KEY: ${MINIO_VOLUME_ACCESS_KEY?Variable not set}
      S3_SECRET_KEY: ${MINIO_VOLUME_SECRET_KEY?Variable not set}
      S3FS_FORCE_REINSTALL: ${S3FS_FORCE_REINSTALL:-false}
      PLUGIN_ALIAS: cluster-s3fs
    command:
      - |
        echo "configuring s3fs volume driver. PLUGIN_ALIAS:$${PLUGIN_ALIAS} S3_URL:$${S3_URL}"
        wait-for "$${S3_URL}/minio/health/ready" -t 10
        set +e
        ENABLED=`docker plugin inspect --format='{{.Enabled}}' $$PLUGIN_ALIAS`
        if [[ "$${ENABLED,,}" != "true" || "$${S3FS_FORCE_REINSTALL,,}" == "true" ]]
        then
          docker plugin rm -f $$PLUGIN_ALIAS
        else
          echo "s3fs volume driver installed and enabled. PLUGIN_ALIAS:$${PLUGIN_ALIAS} S3_URL:$${S3_URL}"
          exit 0
        fi
        echo "s3fs volume driver install started. PLUGIN_ALIAS:$${PLUGIN_ALIAS} S3_URL:$${S3_URL}"
        docker plugin install rexray/s3fs \
        	S3FS_OPTIONS="use_path_request_style,nonempty,allow_other,url=$$S3_URL" \
        	S3FS_ENDPOINT="$$S3_URL" \
        	S3FS_ACCESSKEY="$$S3_ACCESS_KEY" \
        	S3FS_SECRETKEY="$$S3_SECRET_KEY" \
        	--alias $$PLUGIN_ALIAS --grant-all-permissions
        docker plugin enable $$PLUGIN_ALIAS
        exit 0
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    deploy:
      mode: global
      <<: *delayed-on-failure

volumes:
  minio_0_data_0:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-0"
  minio_0_data_1:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-1"
  minio_1_data_0:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-0"
  minio_1_data_1:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-1"
  minio_2_data_0:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-0"
  minio_2_data_1:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/minio/data-1"
  caddy-data:
    driver: local
    driver_opts:
      type: "none"
      o: "bind"
      device: "/mnt/docker/cluster/caddy"

#docker network create --driver=overlay --opt encrypted=true caddy
networks:
  traefik-public:
    external: true
  #minio node to minio node
  minio-internal:
    driver: overlay
    driver_opts:
      encrypted: "true"
  #services to minio node
  cluster-storage:
    name: cluster-storage
    attachable: true
    driver: overlay
    driver_opts:
      encrypted: "true"
  cluster-caddy:
    name: cluster-caddy
    attachable: true
    driver: overlay
    driver_opts:
      encrypted: "true"

secrets:
   cluster_MINIO_ROOT_USER:
     external: true
   cluster_MINIO_ROOT_PASSWORD:
     external: true
   cluster_REDIS_PASSWORD:
     external: true

#DOMAIN=
#EMAIL=
#MINIO_VOLUME_ACCESS_KEY=
#MINIO_VOLUME_SECRET_KEY=
#MINIO_CLUSTER_REDIS_ACCESS_KEY=
#MINIO_CLUSTER_REDIS_SECRET_KEY=
#ROUTE53_ACCESS_KEY_CADDY=
#ROUTE53_SECRET_KEY_CADDY=
#CADDY_REDIS_AES_KEY=
#CADDY_REDIS_PROXY_SERVICE_STORAGE_AES_KEY=
#ZERO_SSL_API_KEY=

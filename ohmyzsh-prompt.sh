(

sed -i '/#TITLE_PROMPT_CODE_START/,/#TITLE_PROMPT_CODE_END/d' ~/.zshrc

source ~/.zshrc

read -r -d '' TITLE_PROMPT_CODE << EOM

#TITLE_PROMPT_CODE_START

DISABLE_AUTO_TITLE="true"
HOSTNAME=$(hostname)
TITLE_PRE="\033]0;"
TITLE_POST="@$HOSTNAME\007"

window_title="$TITLE_PRE${PWD##*/}$TITLE_POST"
echo -ne "$window_title"

function chpwd () {
  window_title="$TITLE_PRE${PWD##*/}$TITLE_POST"
  echo -ne "$window_title"
}

function set-title(){
  TITLE="\[\e]2;$*\a\]"
  echo -e ${TITLE}
}

PROMPT="$fg[yellow]%}$USER$fg[white]%}@%{$fg[cyan]%}%m ${PROMPT}"

#TITLE_PROMPT_CODE_END

EOM

echo $TITLE_PROMPT_CODE >> ~/.zshrc

source ~/.zshrc

)
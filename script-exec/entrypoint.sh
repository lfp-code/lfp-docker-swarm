#!/bin/bash

set -e

debug_log () {
	if [[ "${DEBUG_LOG,,}" == "true" ]]
	then
		echo "$1";
	fi
}

SCRIPT="$1"
shift
ARGS="$@"
[[ -z "$SCRIPT" ]] && { echo "script must be passed as first argument" >&2; exit 1; }


debug_log "**************"
debug_log "SCRIPT:"
debug_log $SCRIPT
debug_log "ARGS:"
debug_log $ARGS
debug_log "**************"
debug_log ""

while IFS= read -r line; do
  name=${line%%=*}
  if( ! echo "$name" | grep -i "_FILE$" > /dev/null ); then
    continue
  fi
  value=${line#*=}
  if [ -z "$value" ]; then
    continue
  fi
  if [ ! -e "$value" ]; then
    continue
  fi
  nameNoFile=$(echo ${name%_FILE*})
  valueNoFile="${!nameNoFile}"
  if [ ! -z "$valueNoFile" ]; then
    continue
  fi
  valueFile=$(cat "$value")
  if [ -z "$valueFile" ]; then
    continue
  fi
  debug_log "exporting env variable from file. name:$nameNoFile value:$valueFile file:$value"
  export "$nameNoFile"="$valueFile"
done <<<$(env)

SCRIPT_FILE=$(mktemp)

debug_log "writing script:${SCRIPT_FILE}"
echo "set -e" > $SCRIPT_FILE
echo "$SCRIPT" >> $SCRIPT_FILE
chmod +x $SCRIPT_FILE

debug_log "running script:${SCRIPT_FILE}"
set +e
$SCRIPT_FILE $ARGS
EXIT_CODE=$?
debug_log "exit code:${EXIT_CODE}"

debug_log "deleting script:${SCRIPT_FILE}"
rm $SCRIPT_FILE

exit $EXIT_CODE

# SCRIPT=$(./test-script.sh)
# docker run \
	# -e "DEBUG_LOG=truE" \
	# -v "${PWD}"/entrypoint.sh:/entrypoint.sh \
	# -v /var/run/docker.sock:/var/run/docker.sock \
	# -it regbo/script-exec:latest "$SCRIPT"

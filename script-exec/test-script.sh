echo "DUMMY:$DUMMY"
docker run -it ubuntu bash
docker plugin install --alias s3fs-test \
  mochoa/s3fs-volume-plugin \
  --grant-all-permissions --disable
docker plugin set s3fs-test AWSACCESSKEYID=key
docker plugin set s3fs-test AWSSECRETACCESSKEY=secret
docker plugin enable s3fs-test

#docker plugin rm -f s3fs-test
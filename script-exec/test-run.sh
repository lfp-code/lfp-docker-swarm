SCRIPT=$(cat ./test-script.sh)
docker run \
	-e "DEBUG_LOG=truE" \
	-e "DUMMY_FILE=/dummy.txt" \
	-v "${PWD}"/entrypoint.sh:/entrypoint.sh \
	-v "${PWD}"/dummy.txt:/dummy.txt \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-it regbo/script-exec:latest "$SCRIPT"
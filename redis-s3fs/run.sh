docker run -d --rm \
    --name "${HOST_DOCKER_NAME}" \
    --device /dev/fuse \
    --cap-add SYS_ADMIN \
    --security-opt "apparmor=unconfined" \
	--env "AWS_S3_URL=${S3_URL}" \
    --env "AWS_S3_BUCKET=${S3_BUCKET}" \
    --env "AWS_S3_ACCESS_KEY_ID=${S3_ACCESS_KEY}" \
    --env "AWS_S3_SECRET_ACCESS_KEY=${S3_SECRET_KEY}" \
	--env "S3FS_ARGS=use_path_request_style,allow_other,nonempty" \
    --env "UID=1001" \
    --env "GID=1001" \
	--net "${S3_NETWORK}" \
    -v "${HOST_DATA_DIR}:/opt/s3fs/bucket:rshared" \
    efrecon/s3fs:1.90
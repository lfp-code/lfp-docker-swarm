set -e

[[ -z "$S3_URL" ]] && { echo "S3_URL required"; exit 1; }
[[ -z "$S3_ACCESS_KEY" ]] && { echo "S3_ACCESS_KEY required"; exit 1; }
[[ -z "$S3_SECRET_KEY" ]] && { echo "S3_SECRET_KEY required"; exit 1; }
[[ -z "$S3_BUCKET" ]] && { echo "S3_BUCKET required"; exit 1; }
[[ -z "$S3_BUCKET" ]] && { echo "S3_BUCKET required"; exit 1; }


if [ -z "$S3_HEALTH_CHECK_URL" ]
then
	S3_HEALTH_CHECK_URL="${S3_URL}/minio/health/ready"
fi

echo "wait for: $S3_HEALTH_CHECK_URL"
wait-for $S3_HEALTH_CHECK_URL -t 10

echo "mounting s3fs"

mkdir -p /mnt/s3
echo "${S3_ACCESS_KEY}:${S3_SECRET_KEY}" > /etc/s3cred
chmod 600 /etc/s3cred
s3fs "${S3_BUCKET}" /mnt/s3 -o passwd_file=/etc/s3cred,use_path_request_style,nonempty,allow_other,umask=000,url=$S3_URL
bash wait-for-cmd 'mount | grep s3fs' 10

echo "starting redis"

rm -rf /bitnami/redis/data
mkdir -p /mnt/s3/bitnami/redis/data
ln -s /mnt/s3/bitnami/redis/data /bitnami/redis/data

/opt/bitnami/scripts/redis/entrypoint.sh /opt/bitnami/scripts/redis/run.sh


sudo apt update
sudo apt install -y software-properties-common
sudo add-apt-repository -y ppa:gluster/glusterfs-7
sudo apt update
sudo apt install -y glusterfs-server 

sudo systemctl start glusterd
sudo systemctl enable glusterd

ssh-keygen -q -t rsa -N '' -f /root/.ssh/id_rsa_glusterfs <<<y >/dev/null 2>&1



sudo gluster pool list

gluster peer probe swarm01
gluster peer probe swarm02

mkdir -p /swarm-manager/glusterfs/brick0 /swarm-manager/glusterfs/volume

sudo gluster volume create swarm-manager-glusterfs replica 3 swarm00:/swarm-manager/glusterfs/brick0 swarm01:/swarm-manager/glusterfs/brick0 swarm02:/swarm-manager/glusterfs/brick0 force
sudo gluster volume start swarm-manager-glusterfs

sudo mount.glusterfs localhost:/swarm-manager-glusterfs /swarm-manager/glusterfs/volume

#sudo gluster volume stop swarm-manager-glusterfs

# edit fstab to contain: localhost:/swarm-manager-glusterfs /swarm-manager/glusterfs/volume glusterfs defaults,_netdev,noauto,x-systemd.automount 0 0

FSTAB_APPEND="localhost:/swarm-manager-glusterfs /swarm-manager/glusterfs/volume glusterfs defaults,_netdev,noauto,x-systemd.automount 0 0"
echo $FSTAB_APPEND >> /etc/fstab
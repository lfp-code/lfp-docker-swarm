export DOMAIN=
export EMAIL=
export ACME_EAB_KID=
export ACME_EAB_HMAC_ENCODED=
export AUTH_USERNAME=admin
export AUTH_PASSWORD=
export HASHED_PASSWORD=$(openssl passwd -apr1 $AUTH_PASSWORD)
#$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
export CSYNC2_KEY=